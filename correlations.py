#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""correlations.py

We want:
    - Universalism Agg mean of _r3, r_8, _r19
    - Correlation of _r3, r_8, _r19 with ClimateTransport, SustainableTransport_r*, TrainClimate.
"""

from itertools import combinations
from typing import Tuple

import matplotlib
import matplotlib as mpl
import matplotlib.font_manager as font_manager
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from pyprojroot import here
from scipy import stats
from scipy.stats import pearsonr, spearmanr
from sklearn.feature_selection import f_oneway
from statsmodels.stats.multicomp import pairwise_tukeyhsd


def setup_plotting_parameters(
    resolution: int = 600, size: Tuple[float, float] = (7.8, 5.8)
) -> None:
    # plt.rcParams["figure.figsize"] = size
    # plt.rcParams["savefig.dpi"] = resolution
    mpl.rcParams["font.family"] = "serif"
    cmfont = font_manager.FontProperties(
        fname=mpl.get_data_path() + "/fonts/ttf/cmr10.ttf"
    )
    mpl.rcParams["mathtext.fontset"] = "cm"


def get_col_names():

    messaging_cols = [
        "Messaging_Random1",
        "Messaging_Random2",
        "Messaging_Random3",
        "Messaging_Random4",
        "Messaging_Random5",
        "Messaging_Random6",
        "Messaging_Random7",
        "Messaging_Random8",
        "Messaging_Random9",
        "Messaging_Random10",
        "Messaging_Random11",
        "Messaging_Random12",
        "Messaging_Random13",
        "Messaging_Random14",
        "Messaging_Random15",
    ]
    value_cols = [
        "Value_r1",
        "Value_r2",
        "Value_r3",
        "Value_r4",
        "Value_r5",
        "Value_r6",
        "Value_r7",
        "Value_r8",
        "Value_r9",
        "Value_r10",
        "Value_r11",
        "Value_r12",
        "Value_r13",
        "Value_r14",
        "Value_r15",
        "Value_r16",
        "Value_r17",
        "Value_r18",
        "Value_r19",
        "Value_r20",
        "Value_r21",
    ]

    schultz_scale_cols = [
        "SchultzScale_r1",
        "SchultzScale_r2",
        "SchultzScale_r3",
        "SchultzScale_r4",
        "SchultzScale_r5",
        "SchultzScale_r6",
        "SchultzScale_r7",
        "SchultzScale_r8",
        "SchultzScale_r9",
    ]

    sus_trans_cols = [
        "SustainableTransport_r1",
        "SustainableTransport_r2",
        "SustainableTransport_r3",
        "SustainableTransport_r4",
    ]
    relevant_cols = [
        "English",
        "Age",
        "Gender",
        "Education",
        "Nationality",
        "Residing",
        "Swiss",
        "Religion",
        "Parent",
        "Household",
        "Community",
        "Work",
        "Income",
        "offset",
        "ClimateTransport_r1",
        "trainclimate_r1",
        "people_r1",
        "people_r2",
        "sys_CBCVersion_Messaging",
        "sys_CBCDesignID_Messaging",
    ]

    relevant_cols.extend(messaging_cols)
    relevant_cols.extend(value_cols)
    relevant_cols.extend(schultz_scale_cols)
    relevant_cols.extend(sus_trans_cols)
    return relevant_cols


def clean_emily_data(df):
    # if time spent on last page is not na, then we can keep the row.
    df = df.iloc[df["sys_pagetime_38"].dropna().index]

    # Get relevant cols
    relevant_cols = get_col_names()
    df = df[relevant_cols]

    # remap nationalities

    df = df.replace(
        {
            "Nationality": {
                "Dutch, French": "Dutch",
                "French - Dutch": "Dutch",
                "China": "Chinese",
                "Canada": "Canadian",
                "Swiss / Filipino": "Swiss",
                "german": "German",
                "Switzerland": "Swiss",
                "USA": "American",
                "test": "Swiss",
                "I am swiss": "Swiss",
                "France": "French",
                "CH, DE, Bosnian": "Swiss",
                "Swiss, Indian and Great Birtan": "Swiss",
                "swiss": "Swiss",
                "hungarian": "Hungarian",
                "BELGIAN": "Swiss",
                "Republic of Korea": "South Korean",
                "Swiss/italian": "Swiss",
                "Swiss and Italian": "Swiss",
                "Japan": "Japanese",
                "Germany": "German",
            },
            "Residing": {
                "3 years": 3,
                "1.5 years": 2,
                "10 years": 10,
                "Almost 2 years": 2,
                "13 months": 1,
                "27": 27,
                "20 years": 20,
                "35 Years": 35,
                "21 yrs": 21,
                "26 years": 26,
                "23 years (from birth on)": 23,
                "10 months": 1,
                "2 years": 2,
                "25 years": 25,
                "18years": 18,
                "born in Switzerland and lived here ever since": 18,
                "A few weeks for holidays": 1,
                "No": 1,
                "1 year": 1,
                "9 years": 9,
                "10months": 1,
                "8 month": 1,
                "a month": 1,
                "yes": 18,
                "34 Years": 34,
                "16 Years": 16,
                "23 years": 23,
                "I have never been a swiss resident": 1,
                "31 years": 31,
                "halfe of my life": 18,
                "6 Months": 1,
                "Never": 1,
                "test": 1,
                "8 years": 8,
                "21 years": 21,
                "14 months": 2,
                "11 Years (since 2011)": 11,
                "25 y": 25,
                "27 years": 27,
                "28 years": 28,
                "Since birth": 18,
                "since birth": 18,
                "5 months": 1,
                "I'm born here": 18,
                "since birth, 8.8.03": 18,
                "Since my birth.": 18,
                "All my life (I was born in Fribourg)": 18,
                "my whole life": 18,
                "6 months": 1,
                "5 years": 5,
                "13 years": 13,
                "I was born in Switzerland": 18,
                "3 YEARS": 3,
                "I was born there": 18,
                "30 years": 30,
                "Since 2014": 8,
                "7 years": 7,
                "Since I was born: 20 years.": 20,
                "3,5 years": 3.5,
                "All my life (23 years)": 23,
                "All my life, so 23 years": 23,
                "All my life": 18,
                "Since I was born, so 20 years": 20,
                "Since I was born, so 19 years.": 20,
                "27 years, all my life": 27,
                "19 years": 19,
                "24 Years": 24,
                "25 years (all my life)": 25,
                "5 years 6 months": 6,
                "Since Birth": 18,
            },
        },
    )

    numerical_cols = ["Age", "Residing"]
    categorical_cols = [col for col in df.columns if col not in numerical_cols]
    df[categorical_cols] = df[categorical_cols].astype("category")
    df[numerical_cols] = df[numerical_cols].astype("int")
    return df, categorical_cols, numerical_cols


def get_universalism(df):
    df["universalism"] = (
        df["Value_r3"].astype("int")
        + df["Value_r8"].astype("int")
        + df["Value_r19"].astype("int")
    ) / 3
    return df


def agg_mean_universalism(df):
    df = get_universalism(df)
    universalism_mean = df["universalism"].mean()
    print(universalism_mean)


def correlations_universalism(df):
    df = get_universalism(df)
    correlation, p = spearmanr(
        df.universalism.values.flatten(),
        df.ClimateTransport_r1.astype("int").values.flatten(),
    )
    print(f"Spearman correlation for ClimateTransport_r1")
    print(f"Correlation {correlation}")
    print(f"p-value {p}")

    correlation, p = pearsonr(
        df.universalism.values.flatten(),
        df.ClimateTransport_r1.astype("int").values.flatten(),
    )
    print(f"Pearson correlation for ClimateTransport_r1")
    print(f"Correlation {correlation}")
    print(f"p-value {p}")

    for i in range(4):
        col = f"SustainableTransport_r{i+1}"
        correlation, p = spearmanr(
            df.universalism.values.flatten(),
            df[col].astype("int").values.flatten(),
        )
        print(f"Spearman Correlation for {col}")
        print(f"Correlation {correlation}")
        print(f"p-value {p}")

        correlation, p = pearsonr(
            df.universalism.values.flatten(),
            df[col].astype("int").values.flatten(),
        )
        print(f"Pearson Correlation for {col}")
        print(f"Correlation {correlation}")
        print(f"p-value {p}")

    correlation, p = pearsonr(
        df.universalism.values.flatten(),
        df["trainclimate_r1"].astype("int").values.flatten(),
    )
    print(f"Pearson correlation for trainclimate")
    print(f"Correlation {correlation}")
    print(f"p-value {p}")

    correlation, p = spearmanr(
        df.universalism.values.flatten(),
        df["trainclimate_r1"].astype("int").values.flatten(),
    )
    print(f"Spearman correlation for trainclimate")
    print(f"Correlation {correlation}")
    print(f"p-value {p}")


def scatterplots_variables(df):
    setup_plotting_parameters()
    ax = sns.regplot(
        data=df, x="universalism", y="ClimateTransport_r1", scatter=False
    )
    ax.set(
        xlabel="Universalism", ylabel="Willingness to Increase Train Travel"
    )
    plt.savefig("universalism_climate.pdf")
    df_tmp = df[
        [
            "universalism",
            "SustainableTransport_r1",
            "SustainableTransport_r2",
            "SustainableTransport_r3",
            "SustainableTransport_r4",
        ]
    ]
    df_tmp = df_tmp.rename(
        columns={
            "SustainableTransport_r1": "Plane",
            "SustainableTransport_r2": "Train",
            "SustainableTransport_r3": "Bus",
            "SustainableTransport_r4": "Car",
        },
    )

    df_tmp = df_tmp.melt(id_vars=["universalism"])
    df_tmp = df_tmp.rename(columns={"variable": "Modes of Transport"})
    ax = sns.lmplot(
        data=df_tmp,
        x="universalism",
        y="value",
        hue="Modes of Transport",
        scatter=False,
        palette="bright",
    )
    ax.set(xlabel="Universalism", ylabel="Frequence of Transport Use")
    plt.savefig("universalism_sustrans.pdf")

    plt.clf()

    ax = sns.regplot(
        data=df, x="universalism", y="trainclimate_r1", scatter=False
    )
    ax.set(xlabel="Universalism", ylabel="Reduction of Climate Change")
    plt.savefig("universalism_trainclimate.pdf")


def main():
    emily_data_raw = pd.read_csv(here() / "emily_data.csv")
    emily_data_cleaned, categorical_cols, numerical_cols = clean_emily_data(
        emily_data_raw
    )
    agg_mean_universalism(emily_data_cleaned)
    correlations_universalism(emily_data_cleaned)
    scatterplots_variables(emily_data_cleaned)


if __name__ == "__main__":
    main()
