#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""emily_oversampling.py

Here we perform the oversampling of the data
"""


import matplotlib as mpl
import matplotlib.font_manager as font_manager
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from imblearn.over_sampling import SMOTENC
from pyprojroot import here


def setup_plotting_parameters(
    resolution: int = 600, size: Tuple[float, float] = (7.8, 5.8)
) -> None:
    # plt.rcParams["figure.figsize"] = size
    # plt.rcParams["savefig.dpi"] = resolution
    mpl.rcParams["font.family"] = "serif"
    cmfont = font_manager.FontProperties(
        fname=mpl.get_data_path() + "/fonts/ttf/cmr10.ttf"
    )
    mpl.rcParams["mathtext.fontset"] = "cm"


def get_col_names():

    messaging_cols = [
        "Messaging_Random1",
        "Messaging_Random2",
        "Messaging_Random3",
        "Messaging_Random4",
        "Messaging_Random5",
        "Messaging_Random6",
        "Messaging_Random7",
        "Messaging_Random8",
        "Messaging_Random9",
        "Messaging_Random10",
        "Messaging_Random11",
        "Messaging_Random12",
        "Messaging_Random13",
        "Messaging_Random14",
        "Messaging_Random15",
    ]
    value_cols = [
        "Value_r1",
        "Value_r2",
        "Value_r3",
        "Value_r4",
        "Value_r5",
        "Value_r6",
        "Value_r7",
        "Value_r8",
        "Value_r9",
        "Value_r10",
        "Value_r11",
        "Value_r12",
        "Value_r13",
        "Value_r14",
        "Value_r15",
        "Value_r16",
        "Value_r17",
        "Value_r18",
        "Value_r19",
        "Value_r20",
        "Value_r21",
    ]

    schultz_scale_cols = [
        "SchultzScale_r1",
        "SchultzScale_r2",
        "SchultzScale_r3",
        "SchultzScale_r4",
        "SchultzScale_r5",
        "SchultzScale_r6",
        "SchultzScale_r7",
        "SchultzScale_r8",
        "SchultzScale_r9",
    ]

    sus_trans_cols = [
        "SustainableTransport_r1",
        "SustainableTransport_r2",
        "SustainableTransport_r3",
        "SustainableTransport_r4",
    ]
    relevant_cols = [
        "English",
        "Age",
        "Gender",
        "Education",
        "Nationality",
        "Residing",
        "Swiss",
        "Religion",
        "Parent",
        "Household",
        "Community",
        "Work",
        "Income",
        "offset",
        "ClimateTransport_r1",
        "trainclimate_r1",
        "people_r1",
        "people_r2",
        "sys_CBCVersion_Messaging",
        "sys_CBCDesignID_Messaging",
    ]

    relevant_cols.extend(messaging_cols)
    relevant_cols.extend(value_cols)
    relevant_cols.extend(schultz_scale_cols)
    relevant_cols.extend(sus_trans_cols)
    return relevant_cols


def clean_emily_data(df):
    # if time spent on last page is not na, then we can keep the row.
    df = df.iloc[df["sys_pagetime_38"].dropna().index]

    # Get relevant cols
    relevant_cols = get_col_names()
    df = df[relevant_cols]

    # remap nationalities

    df = df.replace(
        {
            "Nationality": {
                "Dutch, French": "Dutch",
                "French - Dutch": "Dutch",
                "China": "Chinese",
                "Canada": "Canadian",
                "Swiss / Filipino": "Swiss",
                "german": "German",
                "Switzerland": "Swiss",
                "USA": "American",
                "test": "Swiss",
                "I am swiss": "Swiss",
                "France": "French",
                "CH, DE, Bosnian": "Swiss",
                "Swiss, Indian and Great Birtan": "Swiss",
                "swiss": "Swiss",
                "hungarian": "Hungarian",
                "BELGIAN": "Swiss",
                "Republic of Korea": "South Korean",
                "Swiss/italian": "Swiss",
                "Swiss and Italian": "Swiss",
                "Japan": "Japanese",
                "Germany": "German",
            },
            "Residing": {
                "3 years": 3,
                "1.5 years": 2,
                "10 years": 10,
                "Almost 2 years": 2,
                "13 months": 1,
                "27": 27,
                "20 years": 20,
                "35 Years": 35,
                "21 yrs": 21,
                "26 years": 26,
                "23 years (from birth on)": 23,
                "10 months": 1,
                "2 years": 2,
                "25 years": 25,
                "18years": 18,
                "born in Switzerland and lived here ever since": 18,
                "A few weeks for holidays": 1,
                "No": 1,
                "1 year": 1,
                "9 years": 9,
                "10months": 1,
                "8 month": 1,
                "a month": 1,
                "yes": 18,
                "34 Years": 34,
                "16 Years": 16,
                "23 years": 23,
                "I have never been a swiss resident": 1,
                "31 years": 31,
                "halfe of my life": 18,
                "6 Months": 1,
                "Never": 1,
                "test": 1,
                "8 years": 8,
                "21 years": 21,
                "14 months": 2,
                "11 Years (since 2011)": 11,
                "25 y": 25,
                "27 years": 27,
                "28 years": 28,
                "Since birth": 18,
                "since birth": 18,
                "5 months": 1,
                "I'm born here": 18,
                "since birth, 8.8.03": 18,
                "Since my birth.": 18,
                "All my life (I was born in Fribourg)": 18,
                "my whole life": 18,
                "6 months": 1,
                "5 years": 5,
                "13 years": 13,
                "I was born in Switzerland": 18,
                "3 YEARS": 3,
                "I was born there": 18,
                "30 years": 30,
                "Since 2014": 8,
                "7 years": 7,
                "Since I was born: 20 years.": 20,
                "3,5 years": 3.5,
                "All my life (23 years)": 23,
                "All my life, so 23 years": 23,
                "All my life": 18,
                "Since I was born, so 20 years": 20,
                "Since I was born, so 19 years.": 20,
                "27 years, all my life": 27,
                "19 years": 19,
                "24 Years": 24,
                "25 years (all my life)": 25,
                "5 years 6 months": 6,
                "Since Birth": 18,
            },
        },
    )
    # Convert nationalities to int
    idx_nats = range(len(df.Nationality.unique().tolist()))
    nats = df.Nationality.unique().tolist()

    idx_designs = range(len(df.sys_CBCDesignID_Messaging.unique().tolist()))
    designs = df.sys_CBCDesignID_Messaging.unique().tolist()

    mapping_nats = {
        nationality: idx for nationality, idx in zip(nats, idx_nats)
    }
    mapping_cbc_design = {
        design: idx for design, idx in zip(designs, idx_designs)
    }
    df = df.replace(
        {
            "Nationality": mapping_nats,
            "sys_CBCDesignID_Messaging": mapping_cbc_design,
        }
    )

    df = df[df.columns].astype("int64")
    numerical_cols = ["Residing", "Age", "sys_CBCVersion_Messaging"]
    categorical_cols = [col for col in df.columns if col not in numerical_cols]
    df[categorical_cols] = df[categorical_cols].astype("category")

    # We get all rows with more than one response type
    df = df.loc[
        df["sys_CBCVersion_Messaging"].isin(
            df["sys_CBCVersion_Messaging"]
            .value_counts()[df["sys_CBCVersion_Messaging"].value_counts() > 1]
            .index.tolist()
        )
    ]
    y = df["sys_CBCVersion_Messaging"].values

    df = df.loc[:, df.columns != "sys_CBCVersion_Messaging"]
    idx_cat = [df.columns.get_loc(idx) for idx in categorical_cols]
    X = df.values
    return X, y, idx_cat


def main():
    emily_data_raw = pd.read_csv(here() / "emily_data.csv")

    # clean the data
    X, y, idx_cat = clean_emily_data(emily_data_raw)

    sm = SMOTENC(
        random_state=42,
        categorical_features=idx_cat,
        k_neighbors=1,
        sampling_strategy="all",
    )

    # y is gonna be the survey type, x is gonna be the rest of the features.
    X, y = sm.fit_resample(X, y)
    print("Done resampling")


if __name__ == "__main__":
    main()
