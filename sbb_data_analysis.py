#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""sbb_data_analysis.py

Here we conduct the analysis of the sbb data

TODO: ylim
"""

from typing import Tuple

import matplotlib
import matplotlib as mpl
import matplotlib.font_manager as font_manager
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from pyprojroot import here


def setup_plotting_parameters(
    resolution: int = 600, size: Tuple[float, float] = (7.8, 5.8)
) -> None:
    # plt.rcParams["figure.figsize"] = size
    # plt.rcParams["savefig.dpi"] = resolution
    mpl.rcParams["font.family"] = "serif"
    cmfont = font_manager.FontProperties(
        fname=mpl.get_data_path() + "/fonts/ttf/cmr10.ttf"
    )
    mpl.rcParams["mathtext.fontset"] = "cm"


def get_names_of_interest():
    # variable name assignments: Age, Language, Response Type, Destination, Travel Motivation

    age = "age"
    language = "sprache"
    destination = {
        "Berlin": "F4.1.1",
        "Hamburg": "F4.1.2",
        "Hannover": "F4.1.3",
        "Amsterdam": "F4.1.4",
        "Vienna": "F4.1.5",
        "Granz": "F4.1.6",
        "Zagreb": "F4.1.7",
        "Budapest": "F4.1.8",
        "Ljubljana": "F4.1.9",
        "Prague": "F4.1.10",
        "Rome": "F4.1.11",
        "Barcelona": "F4.1.12",
    }
    # response types within each column is 1-5

    motivation = {
        "Change from everyday life": "F4.2.1",
        "New experiences": "F4.2.2",
        "Relax and let go": "F4.2.3",
        "Get to know other people and cultures": "F4.2.4",
        "Bucketlist item": "F4.2.5",
        "Gain experiences": "F4.2.6",
        "Time for yourself and your thoughts": "F4.2.7",
        "Foster friendships": "F4.2.8",
        "Enjoyment (beauty of the country, cuisine, nature experiences)": "F4.2.9",
    }
    # response type vary between 1-7

    # Interest in taking the night train & if they take it again
    interest = "F4.3"

    return age, language, destination, motivation, interest


def destination_vs_age_group(df):
    # Get the names of the variables of interest
    age, language, destination, motivation, interest = get_names_of_interest()

    # Get the data of interest
    df = df[
        [
            age,
            language,
            destination["Berlin"],
            destination["Hamburg"],
            destination["Hannover"],
            destination["Amsterdam"],
            destination["Vienna"],
            destination["Granz"],
            destination["Zagreb"],
            destination["Budapest"],
            destination["Ljubljana"],
            destination["Prague"],
            destination["Rome"],
            destination["Barcelona"],
        ]
    ]

    # Make relplot of barplots
    destination = {value: key for key, value in destination.items()}
    df = df.melt(id_vars=[age, language]).replace({"variable": destination})
    df["count"] = 1
    df = df.groupby(["age", "variable", "value"]).count()
    df = df.reset_index()
    df = df[df["age"] != 4]
    # Unused in case
    df = df[~df["variable"].isin(["Barcelona", "Rome"])]

    width = 0.35  # the width of the bars: can also be len(x) sequence

    # select age group
    titles = ["16-20", "21-25", "26-30", "30+"]
    for idx, j in enumerate(df.age.unique()):
        setup_plotting_parameters()
        plt.rcParams["figure.figsize"] = (9, 5.8)
        ax = plt.subplot()

        df_age = df.loc[df.age == j]

        fig_dict = {}
        color_palette = sns.color_palette("mako", 2)
        custom_palette = [
            sns.color_palette("mako", 2)[0],
            sns.color_palette("rocket", 2)[0],
            sns.color_palette("mako", 2)[1],
            sns.color_palette("rocket", 2)[1],
            sns.color_palette()[7],
        ]
        labels = [
            "Gone already and want to go again",
            "Gone already and would not want to go again",
            "Not gone yet and want to go",
            "Not gone yet and would not want to go",
            "Unsure",
        ]

        ax.bar(
            df_age.variable.unique().tolist(),
            df_age.loc[df_age.value == 1]["count"].values.tolist(),
            width,
            label=labels[0],
            color=custom_palette[0],
        )
        for i in range(2, 6):

            ax.bar(
                df_age["variable"].unique().tolist(),
                df_age.loc[(df_age["value"] == i)]["count"].values.tolist(),
                width,
                bottom=df_age.loc[(df_age["value"] <= i - 1)]
                .groupby(["variable"])
                .sum()["count"]
                .values.tolist(),
                label=labels[i - 1],
                color=custom_palette[i - 1],
            )

        ax.set_ylabel("# respondents")
        ax.set_title(f"Age Group {titles[idx]}")

        # ax.legend(df["value"].unique().tolist())
        patches = []
        for i in range(df_age.value.nunique()):
            patches.append(
                mpatches.Patch(color=custom_palette[i], label=labels[i])
            )
        plt.legend(handles=patches, frameon=False, bbox_to_anchor=(1, 1))
        ax.set_xticklabels(
            labels=df_age["variable"].unique().tolist(), rotation=-45
        )
        ax.legend(
            loc="lower left",
            bbox_to_anchor=(0.035, -0.5),
            fancybox=True,
            ncol=2,
        )

        plt.tight_layout(rect=[0, -0.05, 1, 1])

        plt.savefig(here() / f"destination_vs_age_group_{j}.pdf")
        ax = None
        plt.clf()
        plt.switch_backend(matplotlib.get_backend())
        table_top_preferences = (
            df_age[df_age.value.isin([1, 3])]
            .groupby(["age", "variable"])
            .sum()
            .loc[
                df_age[df_age.value.isin([1, 3])]
                .groupby(["age", "variable"])
                .sum()
                .sprache
                != 0
            ]["count"]
        )
        table_top_preferences.sort_values(ascending=False).to_latex(
            here() / f"table_top_preferences_age_{j}.tex"
        )


def destination_vs_language(df):
    # Get the names of the variables of interest
    age, language, destination, motivation, interest = get_names_of_interest()

    # Get the data of interest
    df = df[
        [
            age,
            language,
            destination["Berlin"],
            destination["Hamburg"],
            destination["Hannover"],
            destination["Amsterdam"],
            destination["Vienna"],
            destination["Granz"],
            destination["Zagreb"],
            destination["Budapest"],
            destination["Ljubljana"],
            destination["Prague"],
            destination["Rome"],
            destination["Barcelona"],
        ]
    ]

    # Make relplot of barplots
    destination = {value: key for key, value in destination.items()}
    df = df.melt(id_vars=[age, language]).replace({"variable": destination})
    df["count"] = 1
    df.variable = df.variable.astype("category")
    df.value = df.value.astype("category")
    df.sprache = df.sprache.astype("category")
    df = df.groupby(["variable", "value", "sprache"]).count()
    df = df.reset_index()
    df = df[df["age"] != 4]
    # Unused in case
    df = df[~df["variable"].isin(["Barcelona", "Rome"])]
    df.variable = df.variable.cat.remove_categories(["Barcelona", "Rome"])
    width = 0.35  # the width of the bars: can also be len(x) sequence

    df.value = df.value.astype("int")

    # select language
    titles = ["German", "French"]
    custom_palette = [
        sns.color_palette("mako", 2)[0],
        sns.color_palette("rocket", 2)[0],
        sns.color_palette("mako", 2)[1],
        sns.color_palette("rocket", 2)[1],
        sns.color_palette()[7],
    ]
    labels = [
        "Gone already and want to go again",
        "Gone already and would not want to go again",
        "Not gone yet and want to go",
        "Not gone yet and would not want to go",
        "Unsure",
    ]
    for idx, j in enumerate(df[language].unique()):
        ax = plt.subplot()

        df_sprache = df.loc[df[language] == j]

        # we add budapest manually here because it is not in the data
        if j == 2:
            line = pd.DataFrame(
                {
                    "variable": "Budapest",
                    "value": 5,
                    "sprache": 2,
                    "age": 4,
                    "count": 4,
                },
                index=[38],
            )
            df_sprache = pd.concat(
                [df_sprache.iloc[:38], line, df_sprache.iloc[38:]]
            )
            df_sprache = df_sprache.sort_values(by=["variable", "value"])

        fig_dict = {}
        color_palette = sns.color_palette("mako_r", df_sprache.value.nunique())
        setup_plotting_parameters()
        ax.bar(
            df_sprache["variable"].unique().tolist(),
            df_sprache.loc[df_sprache["value"] == 1]["count"].values.tolist(),
            width,
            label=labels[0],
            color=custom_palette[0],
        )
        for i in range(2, 6):
            ax.bar(
                df_sprache["variable"].unique().tolist(),
                df_sprache.loc[(df_sprache["value"] == i)][
                    "count"
                ].values.tolist(),
                width,
                bottom=df_sprache.loc[(df_sprache["value"] <= i - 1)]
                .groupby(["variable"])
                .sum()["count"]
                .values.tolist(),
                label=labels[i - 1],
                color=custom_palette[i - 1],
            )

        ax.set_ylabel("# respondents")
        ax.set_title(f"{titles[idx]}")

        # ax.legend(df["value"].unique().tolist())
        patches = []
        for i in range(df_sprache.value.nunique()):
            patches.append(
                mpatches.Patch(color=custom_palette[i], label=labels[i])
            )
        plt.legend(handles=patches, frameon=False, bbox_to_anchor=(1, 1))
        ax.set_xticklabels(
            labels=df_sprache["variable"].unique().tolist(), rotation=-45
        )
        ax.legend(
            loc="lower left",
            bbox_to_anchor=(0.035, -0.5),
            fancybox=True,
            ncol=2,
        )
        plt.tight_layout(rect=[0, -0.05, 1, 1])
        plt.savefig(here() / f"destination_vs_language_{j}.pdf")
        plt.clf()
        plt.switch_backend(matplotlib.get_backend())
        table_top_preferences = (
            df_sprache[df_sprache.value.isin([1, 3])]
            .groupby(["variable"])
            .sum()
            .loc[
                df_sprache[df_sprache.value.isin([1, 3])]
                .groupby(["variable"])
                .sum()["count"]
                != 0
            ]["count"]
        )
        table_top_preferences.sort_values(ascending=False).to_latex(
            here() / f"table_top_preferences_language_{j}.tex"
        )


def motivation_vs_age_group(df):
    # Get the names of the variables of interest
    age, language, destination, motivation, interest = get_names_of_interest()

    # Get the data of interest
    relevant_cols = [age, language]
    relevant_cols.extend(motivation.values())
    df = df[relevant_cols]

    # Make relplot of barplots
    motivation = {value: key for key, value in motivation.items()}
    df = df.melt(id_vars=[age, language]).replace({"variable": motivation})
    df["count"] = 1
    df.variable = df.variable.astype("category")
    df.value = df.value.astype("category")
    df.sprache = df.sprache.astype("category")
    df = df.groupby(["variable", "value", "age"]).count()
    df = df.reset_index()
    df = df[df["age"] != 4]
    # Unused in case
    width = 0.35  # the width of the bars: can also be len(x) sequence

    df.value = df.value.astype("int")
    # select age group
    titles = ["16-20", "21-25", "26-30", "30+"]

    for idx, j in enumerate(df.age.unique()):

        ax = plt.subplot()

        df_age = df.loc[df.age == j]

        setup_plotting_parameters()
        fig_dict = {}
        color_palette = sns.color_palette("mako_r", df_age.value.nunique())
        ax.bar(
            df_age.variable.unique().tolist(),
            df_age.loc[df_age.value == 1]["count"].values.tolist(),
            width,
            label="1",
            color=color_palette[0],
        )
        for i in range(2, 8):

            ax.bar(
                df_age["variable"].unique().tolist(),
                df_age.loc[(df_age["value"] == i)]["count"].values.tolist(),
                width,
                bottom=df_age.loc[(df_age["value"] <= i - 1)]
                .groupby(["variable"])
                .sum()["count"]
                .values.tolist(),
                label=i,
                color=color_palette[i - 1],
            )

        ax.set_ylabel("# respondents")
        ax.set_title(f"Age Group {titles[idx]}")

        # ax.legend(df["value"].unique().tolist())
        patches = []
        for i in range(df_age.value.nunique()):
            patches.append(
                mpatches.Patch(color=color_palette[i], label=f"{i+1}")
            )
        plt.legend(handles=patches, frameon=False, bbox_to_anchor=(1, 1))
        abbreviated_motivations = [
            "Bucketlist",
            "Change",
            "Enjoyment",
            "Friendships",
            "Experiences",
            "People & Cultures",
            "New",
            "Relax",
            "Time",
        ]
        ax.set_xticklabels(labels=abbreviated_motivations, rotation=-90)
        plt.tight_layout(rect=[0, 0.01, 0.92, 1])
        plt.savefig(here() / f"motivation_vs_age_group_{j}.pdf")
        ax = None
        plt.clf()
        plt.switch_backend(matplotlib.get_backend())
        table_top_preferences = (
            df_age[df_age.value.isin([5, 6, 7])]
            .groupby(["variable"])
            .sum()
            .loc[
                df_age[df_age.value.isin([5, 6, 7])]
                .groupby(["variable"])
                .sum()["count"]
                != 0
            ]["count"]
        )
        table_top_preferences.sort_values(ascending=False).to_latex(
            here() / f"table_top_preferences_motivation_age_group_{j}.tex"
        )


def motivation_vs_language(df):
    # Get the names of the variables of interest
    age, language, destination, motivation, interest = get_names_of_interest()

    # Get the data of interest
    relevant_cols = [age, language]
    relevant_cols.extend(motivation.values())
    df = df[relevant_cols]

    # Make relplot of barplots
    motivation = {value: key for key, value in motivation.items()}
    df = df.melt(id_vars=[age, language]).replace({"variable": motivation})
    df["count"] = 1
    df.variable = df.variable.astype("category")
    df.value = df.value.astype("category")
    df.sprache = df.sprache.astype("category")
    df = df.groupby(["variable", "value", language]).count()
    df = df.reset_index()
    df = df[df["age"] != 4]
    # Unused in case
    width = 0.35  # the width of the bars: can also be len(x) sequence

    # select age group
    titles = ["German", "French"]
    for idx, j in enumerate(df[language].unique()):

        ax = plt.subplot()

        df_language = df.loc[df[language] == j]

        motivations = df_language.variable.unique().tolist()
        values = df_language.value.unique().tolist()
        names = ["variable", "value"]

        df_language.variable = df_language.variable.astype("str")
        df_language.value = df_language.value.astype("int")
        df_language[language] = df_language[language].astype("int")
        mind = pd.MultiIndex.from_product(
            [motivations, values],
            names=names,
        )
        df_language = (
            df_language.set_index(names)
            .reindex(mind, fill_value=4)
            .reset_index()
        )
        setup_plotting_parameters()
        fig_dict = {}
        color_palette = sns.color_palette(
            "mako_r", df_language.value.nunique()
        )
        # TODO: USE ITERTOOLS https://stackoverflow.com/questions/31786881/adding-values-for-missing-data-combinations-in-pandas

        ax.bar(
            df_language.variable.unique().tolist(),
            df_language.loc[df_language.value == 1]["count"].values.tolist(),
            width,
            label="1",
            color=color_palette[0],
        )
        for i in range(2, 8):

            ax.bar(
                df_language["variable"].unique().tolist(),
                df_language.loc[(df_language["value"] == i)][
                    "count"
                ].values.tolist(),
                width,
                bottom=df_language.loc[(df_language["value"] <= i - 1)]
                .groupby(["variable"])
                .sum()["count"]
                .values.tolist(),
                label=i,
                color=color_palette[i - 1],
            )

        ax.set_ylabel("# respondents")
        ax.set_title(f"{titles[idx]}")

        # ax.legend(df["value"].unique().tolist())
        patches = []
        for i in range(df_language.value.nunique()):
            patches.append(
                mpatches.Patch(color=color_palette[i], label=f"{i+1}")
            )
        plt.legend(handles=patches, frameon=False, bbox_to_anchor=(1, 1))
        abbreviated_motivations = [
            "Bucketlist",
            "Change",
            "Enjoyment",
            "Friendships",
            "Experiences",
            "People & Cultures",
            "New",
            "Relax",
            "Time",
        ]
        ax.set_xticklabels(labels=abbreviated_motivations, rotation=-90)
        plt.tight_layout(rect=[0, 0.01, 0.92, 1])
        plt.savefig(here() / f"motivation_vs_language_{j}.pdf")
        ax = None
        plt.clf()
        plt.switch_backend(matplotlib.get_backend())
        table_top_preferences = (
            df_language[df_language.value.isin([5, 6, 7])]
            .groupby(["variable"])
            .sum()
            .loc[
                df_language[df_language.value.isin([5, 6, 7])]
                .groupby(["variable"])
                .sum()["count"]
                != 0
            ]["count"]
        )
        table_top_preferences.sort_values(ascending=False).to_latex(
            here() / f"table_top_preferences_motivation_language_{j}.tex"
        )


def interest_vs_age_group(df):
    # Get the names of the variables of interest
    age, language, destination, motivation, interest = get_names_of_interest()

    # Get the data of interest
    relevant_cols = [age, language, interest]
    df = df[relevant_cols]
    df = df.rename(columns={interest: "interest", language: "language"})
    # Make relplot of barplots
    df["count"] = 1
    df.age = df.age.astype("category")
    df.interest = df.interest.astype("category")
    df.language = df.language.astype("category")
    df = df.groupby(["age", "language", "interest"]).count()
    df = df.reset_index()
    df = df[df["age"] != 4]
    df.age = df.age.astype("int")
    df.interest = df.interest.astype("int")
    # Unused in case
    width = 0.35  # the width of the bars: can also be len(x) sequence

    setup_plotting_parameters()
    fig_dict = {}
    color_palette = sns.color_palette("mako_r", df.interest.nunique())
    ax = plt.subplot()
    labels = [
        "Yes, regularly",
        "Yes, sometimes",
        "Yes, once or rarely",
        "No, but would consider",
        "No, would not consider",
    ]
    ax.bar(
        ["16-20", "21-25", "26-30"],
        (
            pd.DataFrame(
                df.loc[df.interest == 1]
                .groupby(["age"])
                .sum()["count"]
                .tolist()
            )
            / pd.DataFrame(df.groupby(["age"]).sum()["count"].tolist())
        )[0].tolist(),
        width,
        label=labels[0],
        color=color_palette[0],
    )
    for i in range(2, 6):
        ax.bar(
            ["16-20", "21-25", "26-30"],
            (
                pd.DataFrame(
                    df.loc[df.interest == i]
                    .groupby(["age"])
                    .sum()["count"]
                    .tolist()
                )
                / pd.DataFrame(df.groupby(["age"]).sum()["count"].tolist())
            )[0].tolist(),
            width,
            bottom=(
                pd.DataFrame(
                    df.loc[(df["interest"] <= i - 1)]
                    .groupby(["age"])
                    .sum()["count"]
                    .values.tolist()
                )
                / pd.DataFrame(df.groupby(["age"]).sum()["count"].tolist())
            )[0].tolist(),
            label=labels[i - 1],
            color=color_palette[i - 1],
        )

    ax.set_ylabel("Fraction of respondents")
    ax.set_title(f"Interest in taking public transport by age group")

    # ax.legend(df["value"].unique().tolist())
    patches = []
    for i in range(df.interest.nunique()):
        patches.append(mpatches.Patch(color=color_palette[i], label=labels[i]))
    plt.legend(handles=patches, frameon=False, bbox_to_anchor=(1, 1))
    # ax.set_xticklabels(labels=df["interest"].unique().tolist(), rotation=-90)
    plt.tight_layout(rect=[0, 0, 1, 1])
    plt.savefig(here() / f"interest_vs_age_groups.pdf")
    ax = None
    plt.clf()
    plt.switch_backend(matplotlib.get_backend())
    fracs = list()
    for i in range(1, 6):
        fracs.append(
            (
                pd.DataFrame(
                    df.loc[df.interest == i]
                    .groupby(["age"])
                    .sum()["count"]
                    .tolist()
                )
                / pd.DataFrame(df.groupby(["age"]).sum()["count"].tolist())
            )[0].tolist()
        )
    fracs = pd.DataFrame(fracs)
    fracs["value"] = range(1, 6)
    fracs.to_latex(here() / "table_interest_vs_age_groups.tex")


def interest_vs_language(df):
    # Get the names of the variables of interest
    age, language, destination, motivation, interest = get_names_of_interest()

    # Get the data of interest
    relevant_cols = [age, language, interest]
    df = df[relevant_cols]
    df = df.rename(columns={interest: "interest", language: "language"})
    # Make relplot of barplots
    df["count"] = 1
    df.age = df.age.astype("category")
    df.interest = df.interest.astype("category")
    df.language = df.language.astype("category")
    df = df.groupby(["age", "language", "interest"]).count()
    df = df.reset_index()
    df = df[df["age"] != 4]
    df.age = df.age.astype("int")
    df.interest = df.interest.astype("int")
    # Unused in case
    width = 0.35  # the width of the bars: can also be len(x) sequence
    labels = [
        "Yes, regularly",
        "Yes, sometimes",
        "Yes, once or rarely",
        "No, but would consider",
        "No, would not consider",
    ]
    setup_plotting_parameters()
    fig_dict = {}
    color_palette = sns.color_palette("mako_r", df.interest.nunique())
    ax = plt.subplot()
    ax.bar(
        ["0", "1"],
        (
            pd.DataFrame(
                df.loc[df.interest == 1]
                .groupby(["language"])
                .sum()["count"]
                .tolist()
            )
            / pd.DataFrame(df.groupby(["language"]).sum()["count"].tolist())
        )[0].tolist(),
        width,
        label="1",
        color=color_palette[0],
    )
    for i in range(2, 6):
        ax.bar(
            ["0", "1"],
            (
                pd.DataFrame(
                    df.loc[df.interest == i]
                    .groupby(["language"])
                    .sum()["count"]
                    .tolist()
                )
                / pd.DataFrame(
                    df.groupby(["language"]).sum()["count"].tolist()
                )
            )[0].tolist(),
            width,
            bottom=(
                pd.DataFrame(
                    df.loc[(df["interest"] <= i - 1)]
                    .groupby(["language"])
                    .sum()["count"]
                    .values.tolist()
                )
                / pd.DataFrame(
                    df.groupby(["language"]).sum()["count"].tolist()
                )
            )[0].tolist(),
            label=labels[i - 1],
            color=color_palette[i - 1],
        )

    ax.set_ylabel("Fraction of respondents")
    ax.set_title(f"Interest in taking public transport by language")

    # ax.legend(df["value"].unique().tolist())
    patches = []
    for i in range(df.interest.nunique()):
        patches.append(mpatches.Patch(color=color_palette[i], label=labels[i]))
    plt.legend(handles=patches, frameon=False, bbox_to_anchor=(1, 1))
    ax.set_xticklabels(labels=["German", "French"])
    plt.tight_layout(rect=[0, 0, 1, 1])
    plt.savefig(here() / f"interest_vs_language.pdf")
    ax = None
    plt.clf()
    plt.switch_backend(matplotlib.get_backend())
    fracs = list()
    for i in range(1, 6):
        fracs.append(
            (
                pd.DataFrame(
                    df.loc[df.interest == i]
                    .groupby(["language"])
                    .sum()["count"]
                    .tolist()
                )
                / pd.DataFrame(
                    df.groupby(["language"]).sum()["count"].tolist()
                )
            )[0].tolist()
        )
    fracs = pd.DataFrame(fracs)
    fracs["value"] = range(1, 6)
    fracs.to_latex(here() / "table_interest_vs_language.tex")


def main():
    sbb_data = pd.ExcelFile(here() / "sbb_data.xlsx")

    # Figure of age groups and languages.
    df_raw_sbb = sbb_data.parse(sbb_data.sheet_names[3])
    age, language, destination, motivation, interest = get_names_of_interest()

    df = df_raw_sbb.copy()
    df[age] = pd.cut(
        df[age], bins=[15, 20, 25, 30, np.inf], labels=[1, 2, 3, 4]
    )
    df = df[df[age] != 4]
    destination_vs_age_group(df)
    destination_vs_language(df)
    motivation_vs_age_group(df)
    motivation_vs_language(df)
    interest_vs_age_group(df)
    interest_vs_language(df)


if __name__ == "__main__":
    main()
